<?php require APPROOT.'/views/includes/header.php';?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-4"><h3>Twiks</h3></div>
        <div class="col-4 text-right">
            <p class="pb-2"><a href=<?php echo URLROOT."/posts/".$_SESSION['user_id']."/view_all" ; ?>><i class="fas fa-user"></i> <b><?php echo $_SESSION['user_name']?></b></a></p>
        </div>
    </div>
    <br>
    <form action="<?php echo URLROOT?>/posts/create" method="post">
        <div class="row justify-content-center"><div class="col-8"><?php echo flash('empty_twik')?></div></div>
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="form-group">
                    <textarea class="form-control" id="twik" name="twik" rows="6" placeholder="Post a twik now..."></textarea>
                </div>
            </div>
            <div class="col-8 text-right">
                <button class="btn btn-primary" type="submit">Post Twik</button>
            </div>
        </div>
    </form>
</div><hr>
<?php foreach($data['posts'] as $post) : ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="card card-body mb-3">
                    <div class="bg-light p-1 mb-3 text-muted">
                        <div class="row">
                            <div class="col-8">Posted by <?php echo $post->first_name; ?> at <?php echo $post->created_at; ?></div>
                            <?php if($post->user_id == $_SESSION['user_id']){
                                echo '<div class="col-4 text-right">
                                        <a href="'.URLROOT.'/posts/show/'.$post->twikId.'" class="btn btn-default"><i class="far fa-edit"></i></a>
                                        <a href="'.URLROOT.'/posts/delete/'.$post->twikId.'" class="btn btn-default"><i class="fas fa-trash"></i></a>
                                     </div>';
                            }
                            ?>    
                        </div>
                    </div>
                    <h5><?php echo $post->twik; ?></h5>  
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php require APPROOT.'/views/includes/footer.php';?>