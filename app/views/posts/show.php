<?php require APPROOT.'/views/includes/header.php';?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-4"><h3>Edit Twiks</h3></div>
        <div class="col-4 text-right">
            <p class="pb-2"><a href=<?php echo URLROOT."/posts/index" ; ?>><i class="fas fa-backward"></i> <b>Back</b></a></p>
        </div>
    </div>
    <br>
    <form action="<?php echo URLROOT.'/posts/update/'.$data['post']->id?>" method="post">
        <div class="row justify-content-center"><div class="col-8"><?php echo flash('empty_twik')?></div></div>
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="form-group">
                    <textarea class="form-control" id="twik" name="twik" rows="6"><?php echo $data['post']->twik; ?></textarea>
                </div>
            </div>
            <div class="col-8 text-right">
                <button class="btn btn-primary" type="submit">Update Twik</button>
            </div>
        </div>
    </form>
</div><hr>
<?php require APPROOT.'/views/includes/footer.php';?>