<?php require APPROOT.'/views/includes/header.php';?>
 <div class="row">
    <div class="col-md-5 col-sm-5 mx-auto">
        <div class="card card-body bg-light mt-3">
            <?php flash("registration_success") ?>
            <h3 class="text-center">LOGIN TO YOUR ACCOUNT</h3><hr>
            <form action="<?php echo URLROOT?>/users/login" method="post">    
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email" value="<?php echo $data['email']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['email_err']?></span>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password"
                            class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['password_err']?></span>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-success btn-lg btn-block" type="submit">Login</button> 
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-default btn-lg btn-block" href="<?php echo URLROOT?>/users/new">No account? Register here.</a> 
                    </div>
                </div>
                
            </form>
        </div>
    </div>
 </div>
<?php require APPROOT.'/views/includes/footer.php';?>