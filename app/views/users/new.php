<?php require APPROOT.'/views/includes/header.php';?>
 <div class="row">
    <div class="col-md-6 col-sm-6 mx-auto">
        <div class="card card-body bg-light mt-3">
            <h3 class="text-center">REGISTER YOUR ACCOUNT</h3><hr>
            <form action="<?php echo URLROOT?>/users/create" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" id="fname" name="first_name" value="<?php echo $data['first_name']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['first_name_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['first_name_err']?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" id="lname" name="last_name" value="<?php echo $data['last_name']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['last_name_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['last_name_err']?></span>
                        </div>
                    </div>
                </div>     
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email" value="<?php echo $data['email']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['email_err']?></span>
                        </div>
                    </div>
                </div>     
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="contact">Contact No.</label>
                            <input type="text" id="contact" name="contact" value="<?php echo $data['contact']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['contact_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['contact_err']?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="text" id="datepicker" name="dob" value="<?php echo $data['dob']?>" autocomplete="off"
                            class="form-control form-control-lg <?php echo (!empty($data['dob_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['dob_err']?></span>
                        </div>
                    </div>
                </div>     
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" value="<?php echo $data['password']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['password_err']?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password</label>
                            <input type="password" id="confirm_password" name="confirm_password" value="<?php echo $data['confirm_password']?>"
                            class="form-control form-control-lg <?php echo (!empty($data['confirm_password_err'])) ? 'is-invalid' : ''?>">
                            <span class="invalid-feedback"><?php echo $data['confirm_password_err']?></span>
                        </div>
                    </div>
                </div>    

                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-success btn-lg btn-block" type="submit">Register</button> 
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-default btn-lg btn-block" href="<?php echo URLROOT?>/users/login">Already have an account? Login</a> 
                    </div>
                </div>
                
            </form>
        </div>
    </div>
 </div>
<?php require APPROOT.'/views/includes/footer.php';?>