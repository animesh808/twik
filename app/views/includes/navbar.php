<nav class="navbar navbar-expand-lg navbar-light mb-3" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo URLROOT?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">About</a>
        </li> 
      </ul>

      <ul class="navbar-nav text-right">
        <li class="nav-item">
          <?php 
            if(isset($_SESSION['user_id'])){
              echo '<a class="nav-link" href="'.URLROOT.'/users/logout">Sign Out</span></a>' ;
            }else{
              echo '<a class="nav-link" href="'.URLROOT.'/users/login">Sign In</span></a>' ;
            }
          ?>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT?>/users/new">Sign Up</span></a>
        </li> 
      </ul>
    </div>
  </nav>