<?php
    function flash($name='', $message='', $type="success"){
        $class = ($type == 'success') ? 'alert alert-success' : 'alert alert-danger';
        if(!empty($name) && !empty($message)){
            unset($_SESSION['name']);
            unset($_SESSION[$name. '_class']);

            $_SESSION['name'] = $message;
            $_SESSION[$name. '_class'] = $class;
        }elseif(!empty($_SESSION['name']) && empty($message)){
            $class = !empty($_SESSION[$name. '_class']) ? $_SESSION[$name. '_class'] : '';
            echo '<div class="'.$class.'">"'.$_SESSION['name'].'"</div>';
            unset($_SESSION['name']);
            unset($_SESSION[$name. '_class']);
        }
    }
?>