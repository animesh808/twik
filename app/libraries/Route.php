<?php

class Route {
    protected $currentController = "PagesController";
    protected $currentMethod = "index";
    protected $params = [];
    
    public function __construct(){
        // print_r($this->getUrl());
        $url = is_null($this->getUrl()) ? ['ne'] : $this->getUrl();
        //check for controller class
        if(file_exists('../app/controllers/'.ucwords($url[0]).'Controller.php')){
            $this->currentController = ucwords($url[0]).'Controller';
            unset($url[0]);
        }
        // require the controller
        require_once '../app/controllers/'.$this->currentController.'.php';
        // instantiate the controller
        $this->currentController = new $this->currentController;

        //check for controller class method
        if(isset($url[1])){
            if(method_exists($this->currentController, $url[1])){
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }

        //get params from url
        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }

    public function getUrl(){
        if (isset($_GET['url'])) {
            return explode('/',filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
}


?>