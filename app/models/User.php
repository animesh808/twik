<?php 
    Class User{
        private $db;
        public function __construct(){
            $this->db = new Database;
        }

        public function register($data){
            $this->db->query('INSERT INTO users (first_name, last_name, email, dob, contact, password) VALUES(:first_name, :last_name, :email, :dob, :contact, :password)');
            $this->db->bind(':first_name', $data['first_name']);
            $this->db->bind(':last_name', $data['last_name']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':dob', $data['dob']);
            $this->db->bind(':contact', $data['contact']);
            $this->db->bind(':password', password_hash($data['password'], PASSWORD_DEFAULT));

            return $this->db->execute() ? true :false;
        }

        public function findUserByEmail($email){
            $this->result = $this->getUser($email);
            return ($this->db->rowCount() > 0) ? true : false;
        }

        public function login($email, $password){
            $this->result = $this->getUser($email);
            $hashed_password = $this->result->password;
            if(password_verify($password, $hashed_password)){
                return $this->result;
            }else{
                return false;
            }
        }

        private function getUser($email){
            $this->db->query('Select * from users where email = :email');
            $this->db->bind(':email', $email);
            return $this->db->getResult();
        }
    }
?>
