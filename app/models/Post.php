<?php 
    class Post{
        private $db;

        public function __construct(){
            $this->db = new Database; 
        }

        public function getPosts(){
            $this->db->query('
                SELECT *,
                twiks.id as twikId,
                users.id as userId
                FROM twiks
                INNER JOIN users ON users.id = twiks.user_id
                ORDER BY twiks.created_at DESC'
            );
            $results = $this->db->getResults();
            return $results;
        }

        public function createPost($data){
            print_r($data);
            $this->db->query("INSERT INTO twiks (user_id, twik) VALUES(:user_id, :twik)");
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':twik', $data['twik']);
            return $this->db->execute();
        }

        public function getTwik($id){
            $this->db->query("SELECT * FROM twiks WHERE id = :id");
            $this->db->bind(':id', $id);
            return $this->db->getResult();
        }

        public function updateTwik($id, $data){
            $this->db->query("UPDATE twiks SET twik = :twik WHERE id = :id");
            $this->db->bind(':id', $id);
            $this->db->bind(':twik', $data['twik']);
            return $this->db->execute();
        }

        public function deleteTwik($id){
            $this->db->query("DELETE FROM twiks WHERE id = :id");
            $this->db->bind(':id', $id);
            return $this->db->execute();
        }
    }
?>