<?php
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'twik');
    define('DB_USER', 'root');
    define('DB_PASSWORD', '');

    define('APPROOT', dirname(dirname(__FILE__)));
    define('URLROOT', 'http://localhost/twik');
    define('APPNAME', 'twik');
?>