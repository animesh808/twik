<?php 
    class PostsController extends Controller{
       
        public function __construct(){
            if(!isLoggedIn()){
                redirect('users/login');
            }
            $this->Post = $this->model('Post');
        }

        public function index(){
            $posts = $this->Post->getPosts();
            $data = [
                'posts' => $posts
            ];
            $this->view('posts/index', $data);
        }

        public function create(){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = $this->prepare_twik_data();
            $valid_data = $this->validate_twik_data($data);
            if(isset($_SESSION['user_id']) && empty($valid_data['twik_err'])){
                if($this->Post->createPost($valid_data)){
                    redirect('posts/index');
                }else{
                    flash("empty_twik", "Something went wrong!!!", 'error');
                    redirect('posts/index');
                }
            }else{
                flash("empty_twik", "Please write something to post!!!", 'error');
                redirect('posts/index');
            }
        }

        public function show($id){
            $post = $this->Post->getTwik($id);
            $data = [
                'post' => $post
            ];
            $this->view('posts/show', $data);
        }

        public function update($id){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = $this->prepare_twik_data();
            $valid_data = $this->validate_twik_data($data);
            if(isset($_SESSION['user_id']) && empty($valid_data['twik_err'])){
                if($this->Post->updateTwik($id,$valid_data)){
                    redirect('posts/index');
                }else{
                    flash("empty_twik", "Something went wrong!!!", 'error');
                    $this->view('posts/show', $data);
                }
            }else{
                flash("empty_twik", "Please write something to post!!!", 'error');
                $this->view('posts/show', $data);
            }
        }

        public function delete($id){
            if($this->Post->deleteTwik($id)){
                echo '<script>alert("Your post deleted successfully.")</script>';
            }else{
                echo '<script>alert("Something went wrong.Try again later!!!")</script>';
            }
            redirect('posts/index');
        }

        private function prepare_twik_data(){
            $data = [
                'user_id' => (int)$_SESSION['user_id'],
                'twik' => !empty($_POST['twik']) ? $_POST['twik'] : '',
                'twik_err' => '',
            ];
            return $data;
        }

        private function validate_twik_data($data){
            if(empty($data['twik'])){
                $data['twik_err'] = "Please write something to post";
            }
            return $data;
        }
    }
?>