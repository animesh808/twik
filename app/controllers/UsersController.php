<?php 
    Class UsersController extends Controller{
        public function __construct(){
            $this->User = $this->model("User");
        }

        public function index(){

        }

        public function new(){
            $this->view('users/new', $this->prepare_registration_data());
        }

        public function create(){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = $this->prepare_registration_data();
            $valid_data = $this->validate_registration_data($data);
            if(empty($valid_data['first_name_err']) && empty($valid_data['last_name_err']) && empty($valid_data['email_err']) && empty($valid_data['contact_err']) 
            && empty($valid_data['dob_err']) && empty($valid_data['password_err']) && empty($valid_data['confirm_password_err'])){
                if($this->User->register($data)){
                    flash("registration_success", "Registration successful. Please login to continue.", "success");
                    redirect("users/login");
                }else{
                    flash("registration_failed", "Something went wrong. Please try again.", 'error');
                }
            }else{
                $this->view('users/new', $valid_data);
            }
        }

        public function show(){

        }

        public function edit(){

        }

        public function update(){

        }

        public function delete(){

        }

        public function login(){
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                $data = $this->prepare_login_data();
                $valid_data = $this->validate_login_data($data);
                if(empty($valid_data['email_err']) && empty($valid_data['password_err'])){
                    $loggedInUser = $this->User->login($data['email'], $data['password']);
                    if($loggedInUser){
                        $this->createUserSession($loggedInUser);
                        redirect('posts/index');
                    }else{
                        $data = $this->prepare_login_data();
                        $data['password_err'] = "Incorrect password";
                        $this->view('users/login', $data);
                    }
                }else{
                    $this->view('users/login', $valid_data);
                }
            }else{
                $this->view('users/login', $this->prepare_login_data());
            }
        }

        public function logout(){
            $this->destroyUserSession();
            redirect('users/login');
        }

        private function prepare_registration_data(){
            $data = [
                'first_name' => !empty($_POST['first_name']) ? trim($_POST['first_name']) : '',
                'last_name' => !empty($_POST['last_name']) ? trim($_POST['last_name']) : '',
                'email' => !empty($_POST['email']) ? trim($_POST['email']) : '',
                'contact' => !empty($_POST['contact']) ? trim($_POST['contact']) : '',
                'dob' => !empty($_POST['dob']) ? trim($_POST['dob']) : '',
                'password' => !empty($_POST['password']) ? trim($_POST['password']) : '',
                'confirm_password' => !empty($_POST['confirm_password']) ? trim($_POST['confirm_password']) : '',
                'first_name_err' => '',
                'last_name_err' => '',
                'email_err' => '',
                'contact_err' => '',
                'dob_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];

            return $data;
        }

        private function prepare_login_data(){
            $data = [
                'email' => !empty($_POST['email']) ? trim($_POST['email']) : '',
                'password' => !empty($_POST['password']) ? trim($_POST['password']) : '',
                'email_err' => '',
                'password_err' => ''
            ];

            return $data;
        }

        private function validate_registration_data($data){

            //validate password
            if(empty($data['password'])){
                $data['password_err'] = "Please enter your password";
            }elseif (strlen($data['password']) < 6) {
                $data['password_err'] = "Minimum length of password is 6";
            }

            if(empty($data['confirm_password'])){
                $data['confirm_password_err'] = "Please enter your password";
            }elseif ($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = "Passwords do not match";
            }

            //validate first_name & last_name
            if(empty($data['first_name'])){
                $data['first_name_err'] = "Please enter your first name";
            }

            if(empty($data['last_name'])){
                $data['last_name_err'] = "Please enter your last name";
            }

            //validate email
            if(empty($data['email'])){
                $data['email_err'] = "Please enter your email";
            }else{
                if($this->User->findUserByEmail($data['email'])){
                    $data['email_err'] = "Email is already taken";
                }
            }

            //validate date of birth
            if(empty($data['dob'])){
                $data['dob_err'] = "Please enter your first name";
            }

            //validate contact
            if(empty($data['contact'])){
                $data['contact_err'] = "Please enter your contact number";
            }

            return $data;
        }

        private function validate_login_data($data){

            //validate password
            if(empty($data['password'])){
                $data['password_err'] = "Please enter your password";
            }

            //validate email
            if(empty($data['email'])){
                $data['email_err'] = "Please enter your email";
            }

            if(!$this->User->findUserByEmail($data['email'])){
                $data['email_err'] = "Email not found";
            }

            return $data;
        }

        private function createUserSession($user){
            $_SESSION['user_id'] = $user->id;
            $_SESSION['user_email'] = $user->email;
            $_SESSION['user_name'] = $user->first_name.' '. $user->last_name;
            $_SESSION['first_name'] = $user->first_name;
        }

        private function destroyUserSession(){
            unset($_SESSION['user_id']);
            unset($_SESSION['user_email']);
            unset($_SESSION['user_name']);
            unset($_SESSION['first_name']);
            session_destroy();
        }
    }
?>